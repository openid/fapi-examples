import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import com.nimbusds.jose.*
import com.nimbusds.jose.crypto.RSAEncrypter
import com.nimbusds.jose.crypto.RSASSASigner
import com.nimbusds.jose.jwk.JWK
import com.nimbusds.jwt.JWTClaimsSet
import com.nimbusds.jwt.SignedJWT
import org.bouncycastle.jce.provider.BouncyCastleProvider
import java.security.Security


object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        generateText()
    }

    fun wrap(s: String): String {
        return s.lineSequence()
                .map { l -> l.chunked(80).joinToString("\n") }
                .joinToString("\n")
    }

    fun format(s: String): String {
        return wrap(s).prependIndent("    ")
    }

    fun prettyPrint(s: String): String {
        return GsonBuilder().setPrettyPrinting().create().toJson(JsonParser.parseString(s));
    }

    fun pubKey(key: String): String {
        val jwk = JWK.parse(key);
        return jwk.toPublicJWK().toJSONString()
    }

    fun generateText() {
        val clientKey = """
{
    "p": "98OYvWTveIosEiipNvuyM6RWikrJd4KEejdStu0mZyZUw4uxdTHiTB9OyNtFLFfTwkplhQOTjiKhuixyMmpBBLh4wZlY3I5AIDttodtx06POJrQopB7nnl38Bx-X6HFdQOAhk307m0Z-MYrL_UGC9RFB5-poqun7oJoN_kVEkG0",
    "kty": "RSA",
    "q": "j-dKLdXaofhQnS7OH_ORy-50fdW_v-IqO3C-GijH5X8tcxed5700Y7hrSLBchFVd3lU7BfjNmuWtav40NvY1dXqK05HCYXQZ_tfFmFNg2b7PNxsqEuvyH96S7L8tpNJ3ZxUgXhvJIvP4EYkoQ6Xg6UZ9wYpBKd9wr7gVCNGZhyU",
    "d": "EtoXjJKJ2bHB2wqYY7qv4NigghhMxb18MTZYRudXflfJ1FssgC-RlELVzHAecGDEcAsWFRyhpCSDJM3VDGDqdpBh4GOC_q0qSPN-jj6mNwj2z6otpT-Qxlh7ZGKXg1v6-bvXwsOlWNhzqneJTJkGMvORnaedReErp9LIwd7f-NA522pW0-fvYhrUpBlwWf5pXxKfFaYvZRPkSvw79HhhJp-cwAs8rESmzXD1oJVCKgfIEADUR50SIJMGLf9J57lGgqxhIZ0Al5UhQbt1rll_z4UF9KfmKnxFT70cNxgNnnvDNIdmwM06wZ4AVEkR5Jf-xoDlcueaj73kX8omztY0UQ",
    "e": "AQAB",
    "use": "sig",
    "kid": "client-2020-08-28",
    "qi": "cJVClbZZRIZpqqdCdT0Tv1W7NwHGFIgwFU_1UNhGJYwSDei9qXRbZPifK1rKs4rlIE763wQTQOdv2lloGhWYw7VNCPU__eNdLALxwElqOqtZ9bJyKbYPH97wLtXiyJmKwijreUNmkU4Td49UqsU93g-5nWMjnI5nUqhCL973_lo",
    "dp": "GKM9rA--dHkg-slKUmpjBRmhdW0MbbLv44te0Uvx1q4MHcnlYqcMPs-aMQUE8uzk5NwrDjgV0zuEY5QJ4xOH1aVWdMIAASawH8RpGYBN6GLQnC6sH-3m0Gu-JSMyIu4q_MmM7TK127H92IVZ1ZxPHnmR2TMJJ33PBOBs5c0TjV0",
    "alg": "PS256",
    "dq": "YDR6rSX5oqtGCa-O8mF-KV5j1vSd2wSyw0ZecVKuQUnC8tzpHlKpwbk7UD9HutjvhoJokVeC7Xl7dLo54jy8BKxGP6eE0m4yKuPKIEotx6T_F5I7kKbZGmFMzKYLN9PVK78-AmyFDa8ZO2_80S0Gri4ISOnLItlUlsV0mo62G1k",
    "n": "i0Ybm4TJyErnD5FIs-6sgAdtP6fG631FXbe5gcOGYgn9aC2BS2h9Ah5cRGQpr3aLLVKCRWU6HRfnGseUBOejo57vI-kgab2YsQJSwedAxvtKrIrJlgKn1gTXMNsz-NQd1LyLSV50qJVEy5l9RtsdDzOV8_kLCbzroEL3rc00iqVZBcQiYm8Bx4z0G8LYZ4oMJAG462Mf_znJkKXsuSIH735xnSmx74CC8TOe6G-V0Wi_wVSJ9bHPphSki_kWUtjVGcnyjYuQVE0LRj3qrGPAX9bsVKSqs8T9AM41TB9oV5Sjz5YhggwICvvCCGwil9qhUoQRkeXtWuGCfvCSeTdawQ"
}
""".trimIndent()

        val clientEncryptionKey = """
{
    "kty": "RSA",
    "d": "OjDe8EkZXgvB-Gy5A4EdU8fBuAjdHLMyHKAtMaS_W_joEJHDvZRhIYbh1jAyHYoR3kFMXutCIYpRjDrsUEhjYuVKLm90CVtysoRjjkiXyupcEW3o--X_HBJhKm1Y-0I7LQ-cA7CotJpTVMR2fRTqP1T4FsORAjg9l-fbdpVmeDiZBRbL2zCWmKWhtDpHyy7vbSCRghntihz_M5Hrchk7r8ito_K3dFrV9IZSF9RoEY7kyK5bL36Kpgai44PYCzqOzqP2fteO_rZ9fn-uK59pI3ySo_PgSbJ55n14Nd9Z8m70zE9Z4aIeNDEFspZUhavngRwc7MuJ7f_hVGQ9RFbbkQ",
    "e": "AQAB",
    "use": "enc",
    "kid": "client-enc-2020-08-28",
    "n": "jVc92j0ntTV0V1nwZ3mpGaV2bME4d6AMS2SRrJBM0fLehaTEqDNzGu0warz2SC9bhcBOB5_q3mYBFjmTwWzSbsk6RYETnAgViXg67PgH7Vkx2NCtwgQW3cNdnUZWRNYHsoevkx_Ta1X6Vi9ulebU_BCKjrF-6CjVcGgEsO_S5DKcukGHdf81WlQOq3zGQg4h7MLArrbPSTHHORDsu_87qY9m2EhiYSOBSF5rHsfDo7zWI5FWNG-_HO-CBM005bykIIS1aXCXx1jOW1OrKcp5xv3e-BR6MJTxncZJ4o1GtynJI8kLXRgltLArSOkbzNEr9GjU9lnSSxKLMtRLKkG2Ow"
}
""".trimIndent()

        var serverKey = """
{
    "p": "21ayR0SVW3HWRjntoKWcIM5elSBjwDHSH-2-FnExIBaSSKzeJLa6S6NJPAHl5AmhHK62YkVOfgyVaVhBrk_Y-587wX7Gcn-p9c9ywen41QF4nffGoClWzG10rGKD0NU9T7o7BVH7vHMZXLVX4maqdfVnTTGIinSmoA9SCmHjQ3s",
    "kty": "RSA",
    "q": "wzLgMl4oVRoXhqrSwm9mNlXpU5HzqVqh96MEgY5zIEeauNyrg0KW87U5xSGICZBuqwv8kftKeNOUbNbUhRyEJ66ncxW32zvhnSS-2cCqIA8L5-z2udJ1dsKwRz3FOm4b5QvK5IIgcCZ5MlkLWrz37gUSrX2TjEAi8D82LmwXla0",
    "d": "wV-ok0r93gMc7Z6zYFB7eFAyzwQtNdNHJ-o3k34-LQEczzjAExI63dFa3bmgzISibIfvGudhbTzgTN0SUh6zj-DlTETZ60Gaj3KbJ8GGY8sxjo34ULyVfaKiziMacaowizr29sFnA53EjyPZBxP2Kmg1tSHMzllHXNi9vbhSUw1nq9JR-B83Ighp3D4_ni3GS-dO7hpbevsKcmviPYyhP0SoOiwZs0V4XourKcIi5Nzt9kdfIIrNWlaCiuqpYqZup2BeKwthEmr3l9TLY08EbaALPkumga_qUjhPTw8hdxRrtoYEo8Fu_XU5IzwQ1XP4Ey-RB-AIo6gwXCUrrLbB",
    "e": "AQAB",
    "use": "sig",
    "kid": "server-2020-08-28",
    "qi": "xpHmD0lWDqtYKHkhGR5g4dgJfmsHBlr69kmqPLakFgCsj_0L4T0CqRCtwUy6THI0C4aKXVURYqbeQzSRuu1likZ0IHuWWSfFlDSggA9X62f6A8aXHYu7GEFz1UJef24T6bZzRqjOlP5NrjLtpIFo9miytweq6qC2tWZ-j6TNj2k",
    "dp": "SrNMJFmHRI2QnHo-I-hDNCkUrVVKo7pUBevCQYJraLJHP9kgDwDskbYaywUadn-RD2OxWeagxO0kNayJMgSfsZKZl1zF2VkBWWY9fy1gQasumR4513I8zYK5qbwjiIwXRX_3eehiA9xvVtOsCr9MbsAVXoTwK7duyDRvjHmqHOE",
    "alg": "PS256",
    "dq": "lcrQ6VYANW5UZ29nVPYPxdqftxXlmiyG5nYe2xzpk_fe78XHiG7tUA_eSTy2HEDN7EzCgXQJbjdPS0aAx7VeZOVOcq5T7NVDCUhNfOSwqPFi_ZOGsoqiwYQdqk3osDIXDenCxSBmrCmaD9ztLbCdwfX6o1Aejh4ZsauQdIdWgfE",
    "n": "pz6g0h7Cu63SHE8_Ib4l3hft8XuptZ-Or7v_j1EkCboyAEn_ZCuBrQOmpUIoPKrA0JNWK_fFeZ2q1_26Gvn3E4dQlcOWpiWkKmxAhYCWnNDv3urVgldDp_kw0Dx2H8yn9tmFW28E_WvrZRwHEF5CzigbxlmFIrkniMHRzjyYQTHRU0gW3DRV9MrQQrmP71McvfLPeMBPPgsHgLo7KmUBDoUjsgnwgycEOWPm8MWJ13dpTsVnoWNIFQqVNz1L5pRU3Uoknl0MGoE6v0M9lfgQgzxIX9gSB1VGp5zZRcsnZGU3MFpwBhOWwiCUwqztoX0H5P0g7OWocspHrDn6YOgxHw"
}
""".trimIndent()

        val requestObjectClaims = """
{
  "aud": "https://fapi-as.example.com/",
  "nbf": 1594140030,
  "scope": "openid payments",
  "iss": "52480754053",
  "response_type": "code id_token",
  "redirect_uri": "https://fapi-client.example.org/fapi-as-callback",
  "state": "VgSUIEnflnDxTe1vAtr54o",
  "exp": 1594140390,
  "nonce": "7xDCHviuPMSXJIigkHOcDi",
  "client_id": "52480754053"
}
""".trimIndent()

        Security.addProvider(BouncyCastleProvider())
        val encKey = JWK.parse(clientEncryptionKey).toRSAKey()

        fun ps256Sign(keyStr: String, claims: String): String {
            val key = JWK.parse(keyStr).toRSAKey()
            val signer: JWSSigner = RSASSASigner(key)
            val claimSet = JWTClaimsSet.parse(claims)
            val jwsObject = SignedJWT(
                    JWSHeader.Builder(JWSAlgorithm.PS256).keyID(key.keyID).build(),
                    claimSet)
            jwsObject.sign(signer)
            return jwsObject.serialize()
        }

        fun encrypt(claims: String): String {
            val jweObject = JWEObject(
                    JWEHeader.Builder(JWEAlgorithm.RSA_OAEP_256, EncryptionMethod.A256GCM)
                            .contentType("JWT") // required to indicate nested JWT
                            .keyID(encKey.keyID)
                            .build(),
                    Payload(ps256Sign(clientKey, claims)))
            jweObject.encrypt(RSAEncrypter(encKey))
            return jweObject.serialize()
        }

        val idTokenClaims = """
{
  "sub": "1001",
  "aud": "52480754053",
  "c_hash": "QR2zucfYZkiLrbKBKDVpgQ",
  "s_hash": "9s6CBbOxiKE65d9-Qr0QIQ",
  "auth_time": 1594140090,
  "iss": "https://fapi-as.example.com/",
  "exp": 1594140390,
  "iat": 1594140090,
  "nonce": "7xDCHviuPMSXJIigkHOcDi"
}
""".trimIndent()

        val jarmClaims = """    
{
  "aud": "469180648039051",
  "code": "zwkGac9juLX8F8frapDISi3K2Fwln4qxwyfNII3Cjz0",
  "iss": "https://fapi-as.example.com/",
  "state": "VgSUIEnflnDxTe1vAtr54o",
  "exp": 1594141090
}
""".trimIndent()

        val clientAssertionClaims = """
{
  "sub": "52480754053",
  "aud": "https://fapi-as.example.com/api/token",
  "iss": "52480754053",
  "exp": 1594140151,
  "iat": 1594140091,
  "jti": "4vBctMSkK4wfuOui9Cyc"
}
""".trimIndent()

        val encryptedIdTokenClaims = """    
{
  "sub": "1001",
  "aud": "2334382354153498",
  "acr": "urn:cds.au:cdr:2",
  "c_hash": "BLfy9hvQUZTDq6_KmF4kDQ",
  "s_hash": "9s6CBbOxiKE65d9-Qr0QIQ",
  "auth_time": 1595827190,
  "iss": "https://fapi-as.example.com/",
  "exp": 1595827490,
  "iat": 1595827190,
  "nonce": "7xDCHviuPMSXJIigkHOcDi"
}
""".trimIndent()

        val s = """
## Appendix A. Examples

The following are non-normative examples of various objects compliant with this specification, with line wraps within values for display purposes only.

The examples signed by the client may be verified with the following JWK:

${format(prettyPrint(pubKey(clientKey)))}

The examples signed by the server may be verified with the following JWK:

${format(prettyPrint(pubKey(serverKey)))}

<!--
The code that generated these examples can be found here:

https://gitlab.com/openid/fapi-examples
-->

### A.1 Example request object

${format(ps256Sign(clientKey, requestObjectClaims))}

which when decoded has the following body:

${format(requestObjectClaims)}

### A.2 Example signed id_token for authorization endpoint response

${format(ps256Sign(serverKey, idTokenClaims))}

which when decoded has the following body:

${format(idTokenClaims)}

### A.3 Example signed and encrypted id_token for authorization endpoint response

${format(encrypt(encryptedIdTokenClaims))}

which when decrypted using the following key:

${format(clientEncryptionKey)}

has the following body:

${format(encryptedIdTokenClaims)}

### A.4 Example JARM response

${format(ps256Sign(serverKey, jarmClaims))}

which when decoded has the following body:

${format(jarmClaims)}

### A.5 Example private_key_jwt client assertion

${format(ps256Sign(clientKey, clientAssertionClaims))}

which when decoded has the following body:

${format(clientAssertionClaims)}

"""
        println(s)
    }
}